# Manipulacao String
a = "Ruby"
b = " on "
c = "Rails"

# puts "teste".upcase
# puts "Sei LA".downcase

# a + b + c
# puts a

# a << b << c
# puts a.reverse.capitalize.empty?

# # Exemplo class
# ela e sempre assimno cor onde eu passo as caracteristicas
# de cada objeto quando eu chamar
=begin
class Veiculo
  def initialize(cor)
    @cor = cor
    @velocidade = 0
  end

  def acelera
    @velocidade += 1
  end
end
=end
# como eu crio um novo objeto, olha a cor que eu declarei aqui e na
# nova class que eu criei
# carro = new Veiculo("vermelho")

# professor fazendo
=begin class Veiculo
  attr_accessor :cor
  @@quantidade_de_veiculos = 3

  def initialize(cor)
    @cor = cor
  end
end

carro = Veiculo.new("Azul")
carro.cor = "Rosinha"
puts carro.cor 
=end

# desse jeito o prof quis fazer sem attr entao voce chama a funcao
# denovo
=begin
class Veiculo
  # Leitura
  def cor
    return @cor
  end

  # Escrita
  def cor=(nome_da_cor)
    @cor = nome_da_cor
  end

  @@quantidade_de_veiculos = 5

  def initialize(cor)
    @@quantidade_de_veiculos += 1
    @cor = cor
  end

  # para pegar as quantidades no atributo da Classe tem que fazer
  # no Ruby voce mostrar a quantidade da classe exatamente, somente 
  # pode usar na classe e nao no objeto o SELF
  def self.getQuantidade
    return @@quantidade_de_veiculos
  end

end

veiculo1 = Veiculo.new("Azul")
veiculo2 = Veiculo.new("Vermelho")
veiculo3 = Veiculo.new("Marron")

puts veiculo1.cor
puts veiculo2.cor
puts veiculo3.cor
puts Veiculo.getQuantidade
=end

# # Blocos
# call chama o bloco e & serve para identificar o parametro como
# bloco
=begin
def meu_metodo(&meu_bloco)
  puts "Antes do bloco"
  meu_bloco.call
  puts "Depois do bloco"
end

meu_metodo do
  puts "Dentro do bloco"
end
# ou
def meu_metodo()
  puts "Antes do bloco"
  yield
  puts "Depois do bloco"
end

meu_metodo do
  puts "Dentro do bloco"
end

def teste(&meu_metodo)
  parametro1 = "a"
  parametro2 = "b"
  meu_metodo.call(parametro1, parametro2) 
end

teste do |param1, param2|
  puts "Os parametros recebidos sao #{param1} e #{param2}"
end
=end
# pode usar chave para demarcar o bloco nao precisa ser o begin e and

