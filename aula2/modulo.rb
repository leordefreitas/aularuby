# Modulo

=begin 
module Matematica
  PI = 3.14

  def Matematica.soma(a, b)
    return a + b
  end

  def Matematica.subtracao(a, b)
    return a - b
  end

  def Matematica.multiplicacao(a, b)
    return a * b
  end

  def Matematica.divisao(a, b)
    return a.to_f / b.to_f
  end
end

puts Matematica::soma(1, 2)
puts Matematica::subtracao(1, 2)
puts Matematica::multiplicacao(1, 2)
puts Matematica::divisao(1, 2)
puts Matematica::PI
=end
=begin
module Pessoa
  def nome=(nome)
    @nome = nome
  end

  def nome
    return @nome
  end
end

# include serve para que nos podemos incluir o modulo
# na class entao vc pode criar uma class como modulo
# e so usar o include
class Crianca
  include Pessoa
end

crianca1 = Crianca.new
crianca1.nome = "Izabela"

crianca2 = Crianca.new
crianca2.nome = "Frederico"

puts crianca1.nome
puts crianca2.nome
=end

module Teste
  def diz_ola
    puts "Ola"
  end
end

class Pessoa
end


# o extend serve para dentro do objeto vc usar alguma coisa que esta
# dentro do modulo
pessoa1 = Pessoa.new
pessoa2 = Pessoa.new
pessoa2.extend(Teste)

# puts pessoa1.diz_ola # dar erro pq nao existe nao dei o extends
puts pessoa2.diz_ola