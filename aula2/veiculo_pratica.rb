class Veiculo
  attr_accessor :id, :passegeiros, :motorista, :cor, :peso, :altura, :numero_rodas, :velocidade, :ligado, :montadora
  # os 2 ponto serve para que vc consiga passa r dentro do parentheses as informacoes mais facil
  # criando os id unicos para cada objeto, desse modo que faz
  @@incrementador_id = 0

    def initialize(cor:, peso:, altura:, numero_rodas:, montadora:)
      @id = @@incrementador_id
      @passegeiros = []
      @motorista = ""
      @cor = cor
      @peso = peso
      @altura = altura
      @numero_rodas = numero_rodas
      @velocidade = 0
      @ligado = false
      @montadora = montadora

      @@incrementador_id += 1
  end
   
  # esse sao os metodos eu criando eles
  def ligar
    @ligado = true
    puts "Veiculo Dando o Start"
  end

  def desligar
    @ligado = false
    puts "Veiculo sendo Desligado"
  end

  def acelerar(valor)
    @velocidade += valor
    puts "Acelerando o veiculo"
  end

  def freiar(valor)
    @velocidade -= valor
    puts "Freiando o veiculo"
  end
end

# esse sinal de menor quer sizer heranca, ou seja esta passando os atribuidos
class Carro < Veiculo
  attr_accessor :tipo

  # o super que dizer que ele esta pegando todas as informacoes e passando para o que vc quer
  # lembrando que no super so pode os atributos que estao no super Classe, as que voce quer
  # so adicionei o tipo pois foi o que eu quis adicionar podia adcionar mais
  def initialize(cor:, peso:, altura:, numero_rodas:, montadora:, tipo:)
    super(cor: cor, peso: peso, altura: altura, numero_rodas: numero_rodas, montadora: montadora)
    @tipo = tipo
  end
end

class Aviao < Veiculo
  attr_accessor :tamanho_asa

  def initialize(cor:, peso:, altura:, numero_rodas:, montadora:, tamanho_asa:)
    super(cor: cor, peso: peso, altura: altura, numero_rodas: numero_rodas, montadora: montadora)
    @tamanho_asa = tamanho_asa
  end

  def voar
    @velocidade += 5
  end

  def levantar_trem_pouso
    puts "Levantando trem de pouso"
  end

  def abaixando_trem_pouso
    puts "Abaixando trem de pouso"
  end
end

# se e os mesmos que vc quer nao tem nenhum novo vc passa so o super
# isolado sem precisar passar nada
class Lancha < Veiculo
  def initialize(cor:, peso:, altura:, numero_rodas:, montadora:)
    super
  end

  def jogar_ancora
    puts "Ancora sendo jogada"
  end
end

# Veiculo
# veiculo = Veiculo.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 4, montadora: "Mercedez")
# veiculo.ligar
# veiculo.acelerar(10)
# veiculo.freiar(10)
# veiculo.desligar
# puts veiculo.velocidade

# Carro
# carro = Carro.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 4, montadora: "Mercedez", tipo: "sport") 
# carro.ligar
# carro.acelerar(10)
# carro.freiar(10)
# carro.desligar
# puts carro.velocidade
# puts carro.tipo

# Aviao
# aviao = Aviao.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 2, montadora: "Mercedez", tamanho_asa: 10)
# aviao.ligar
# aviao.acelerar(10)
# aviao.freiar(10)
# aviao.desligar
# aviao.abaixando_trem_pouso
# aviao.levantar_trem_pouso
# puts aviao.velocidade
# puts aviao.tamanho_asa
    
# Lancha
# lancha = Lancha.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 0, montadora: "Mercedez")
# lancha.ligar
# lancha.acelerar(10)
# lancha.freiar(10)
# lancha.desligar
# lancha.jogar_ancora
# puts lancha.velocidade
# puts lancha.peso

lancha = Lancha.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 0, montadora: "Mercedez")
lancha1 = Lancha.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 0, montadora: "Mercedez")
carro = Carro.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 4, montadora: "Mercedez", tipo: "sport") 
carro1 = Carro.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 4, montadora: "Mercedez", tipo: "sport") 
veiculo = Veiculo.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 4, montadora: "Mercedez")
veiculo1 = Veiculo.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 4, montadora: "Mercedez")
aviao = Aviao.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 2, montadora: "Mercedez", tamanho_asa: 10)
aviao1 = Aviao.new(cor: "Azul", peso: 100, altura: 1.5, numero_rodas: 2, montadora: "Mercedez", tamanho_asa: 10)

print "id do lancha: #{lancha.id}\n\n"
print "id do lancha1: #{lancha1.id}\n\n"
print "id do carro: #{carro.id}\n\n"
print "id do carro1: #{carro1.id}\n\n"
print "id do veiculo: #{veiculo.id}\n\n"
print "id do veiculo1: #{veiculo1.id}\n\n"
print "id do aviao: #{aviao.id}\n\n"
print "id do aviao1: #{aviao1.id}\n\n"