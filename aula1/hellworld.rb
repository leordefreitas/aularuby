# puts "Digite alguma coisa"
# palavra = gets.chomp #chomp e para se livrar do barra na hora de usar o if
# puts palavra

# puts 'Sua Idade:'
# entrada = gets.to_i
# print entrada.class

=begin
puts "name".object_id
puts "name".object_id
puts "name".object_id

puts :name.object_id
puts :name.object_id
puts :name.object_id
=end

# Hash
# aluno = {:name => "Leonardo", :idade => 20, :nota1 => 5.0, :nota2 => 4.0}
=begin puts aluno[:name]
puts aluno[:idade]
puts aluno[:nota1]
puts aluno[:nota2]

print [:name]
print [:idade]
print [:nota1]
print [:nota2] 
=end


# Operadores Relacionais
=begin comparando = aluno[:name] == "Leonard"
puts comparando
comparando = aluno[:nota1] <=> 6
puts comparando
comparando = aluno[:idade] < 21
puts comparando
=end

# Operadores Logicos
=begin comparando = aluno[:name] == "Leonardo" and aluno[:idade] > 19
puts comparando
comparando = aluno[:name] == "Leonardo" or aluno[:idade] > 19
puts comparando
=end

# Condicionais
# x = gets.chomp.to_i
# puts x

# if(x > 0)
#   puts "x e maior que 0"
# elsif(x < 0)
#   puts "x e menor que 0"
# else(x == 0)
#   puts "x e igual 0"
# end

#  puts x >= 10 ? "x e maior ou igual a 10" : "x nao e maior que 10"

# Repeticao
# while(x < 10)
#   x += 1
#   puts x
# end
# y = 0
# begin 
#   puts y
#   y += 1
# end while(y < 10)
# for number in 1..5
#   puts number
# end

# nome e cada elemento como ele chama e do e fazer em ingles
# entao essa e a sintaxe
# array = ["Leo", "Fernando", "Tiago"]
# array.each do |nome|
#   nome_completo = nome + " Silva da Silva"
#   puts nome_completo
# end
 