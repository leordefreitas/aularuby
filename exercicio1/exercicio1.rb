# Exercicio 1
array_exer_1 = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
soma = 0
multiplicacao = 1

array_exer_1.each do |arr|
  arr.each do |num|
    soma += num
    multiplicacao *= num
  end
end

puts "Soma: #{soma}"
puts "Multiplicacao: #{multiplicacao}"
