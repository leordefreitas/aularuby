# Exercicio 3

hash_exer_3 = {:chave1 => 5, :chave2 => 30, :chave3 => 20}

elevado_1 = hash_exer_3[:chave1] ** 2
elevado_2 = hash_exer_3[:chave2] ** 2
elevado_3 = hash_exer_3[:chave3] ** 2

puts "[#{elevado_1}, #{elevado_2}, #{elevado_3}]"
