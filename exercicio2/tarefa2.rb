class Usuario
  attr_accessor :email, :senha, :nome, :nascimento, :logado
    def initialize(email:, senha:, nome:, nascimento:)
      @nome = nome
      @email = email
      @senha = senha
      @nascimento = nascimento
      @logado = false
    end

  def idade
    dt = Time.mktime(@nascimento)
    atual = Time.now
    return ((atual - dt) / 31104000).to_i
  end

  def logar(senha)
    if(senha == @senha)
      @logado = true
    end
  end

  def deslogar
    @logado = false
  end
end


class Aluno < Usuario
  attr_accessor :matricula, :periodo_letivo, :curso, :turmas, :matricula, :periodo_letivo, :curso, :turmas
    def initialize(email:, senha:, nome:, nascimento:, matricula:, periodo_letivo:, curso:)
      super(email: email, senha: senha, nome: nome, nascimento: nascimento)
      @matricula = matricula
      @periodo_letivo = periodo_letivo
      @curso = curso
      @turmas = []
    end

  def inscrever(nome_turma)
    @turmas.push(nome_turma)
  end      
end

class Professor < Usuario
  attr_accessor :matricula, :salario, :materias
    def initialize(email:, senha:, nome:, nascimento:, matricula:, salario:)
      super(email: email, senha: senha, nome: nome, nascimento: nascimento)
      @matricula = matricula
      @salario = salario
      @materias = []
    end
  
  def adicionar_materia(nome_materia)
    @materias.push(nome_materia)
  end
end

class Turma
  attr_accessor :nome, :horario, :dias_da_semana, :inscritos, :inscricao_aberta
    def initialize(nome:, horario:, dias_da_semana:)
      @nome = nome
      @horario = horario
      @dias_da_semana = dias_da_semana
      @inscritos = []
      @inscricao_aberta = false
    end

  def abrir_inscricao 
    @inscricao_aberta = true
  end

  def fechar_inscricao
    @inscricao_aberta = false
  end

  def adicionar_aluno(nome_aluno)
    @inscritos.push(nome_aluno)
  end
end

class Materia
  attr_accessor :ementa, :nome, :professores
    def initialize(ementa:, nome:)
      @ementa = ementa
      @nome = nome
      @professores = []
    end
  
  def adicionar_professor(nome_professor)
    @professores.push(nome_professor)
  end
end

aluno = Aluno.new(email: "aluno@teste.com", senha: "12345", nome: "Aluno", nascimento: "1995/05/10", matricula: "999", periodo_letivo: "manha", curso: "SI")

professor = Professor.new(email: "professor@teste.com", senha: "12345", nome: "Professor", nascimento: "1995/05/10", matricula: "555", salario: 2000)

turma = Turma.new(nome: "Tricotar", horario: "manha", dias_da_semana: "segunda e terca")

materia = Materia.new(ementa: "Nao sei o que e", nome: "Ruby 2")

# Aluno
# puts aluno.email
# puts aluno.senha
# puts aluno.nome
# puts aluno.nascimento
# puts aluno.logado
# puts aluno.matricula
# puts aluno.periodo_letivo
# puts aluno.curso
# puts aluno.idade
# puts aluno.logar("12345")
# puts aluno.deslogar
# puts aluno.inscrever("Jogar Dota 2")
# puts aluno.inscrever("Jogar Dota 2")
# puts aluno.inscrever("Jogar Dota 2")
# puts aluno.turmas


# Professor
# puts professor.email
# puts professor.senha
# puts professor.nome
# puts professor.nascimento
# puts professor.logado
# puts professor.matricula
# puts professor.salario
# puts professor.adicionar_materia("blvblbllbblb")
# puts professor.adicionar_materia("blvblbllbblb")
# puts professor.materias

# Turma
# puts turma.horario
# puts turma.nome
# puts turma.dias_da_semana
# puts turma.inscricao_aberta
# puts turma.abrir_inscricao
# puts turma.fechar_inscricao
# puts turma.adicionar_aluno("zezinho")
# puts turma.adicionar_aluno("neymar de moicano")
# puts turma.inscritos

# Materia
# puts materia.ementa
# puts materia.nome
# puts materia.adicionar_professor("Neymar pai")
# puts materia.adicionar_professor("Neymar filho")
# print materia.professores